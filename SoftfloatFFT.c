#include "fft.h"
#include "softfloat.h"

void fftSoftFloat(uint32_t n, float32_t* input, float32_t* output, int iflg) {
	int i, j;
	int flg = 0;
	int k = n / 2;
	float32_t tr, ti, ww[2], tw[2], tempsoftfloat;
	float tempfloat, th;

	if (iflg >= 0)
		th = -PI;
	else
		th = PI;

	memcpy(output, input, n * 2 * sizeof(float));

	while (k >= 1) {
		tempfloat = sinf(th / (2 * k)) * sinf(th / (2 * k)) * -2;
		memcpy(&ww[0].v, &tempfloat, sizeof(float));
		tempfloat = sinf(th / k);
		memcpy(&ww[1].v, &tempfloat, sizeof(float));

		tw[0] = i32_to_f32(1);
		tw[1] = i32_to_f32(0);

		for (j = 0; j < k; j++) {
			for (i = 1; i <= n; i += (2 * k)) {
				tr = f32_sub(output[(i + j - 1) * 2], output[(i + j + k - 1) * 2]);
				ti = f32_sub(output[(i + j - 1) * 2 + 1], output[(i + j + k - 1) * 2 + 1]);

				output[(i + j - 1) * 2] = f32_add(output[(i + j - 1) * 2], output[(i + j + k - 1) * 2]);
				output[(i + j - 1) * 2 + 1] = f32_add(output[(i + j - 1) * 2 + 1], output[(i + j + k - 1) * 2 + 1]);

				output[(i + j + k - 1) * 2] = f32_sub(f32_mul(tw[0], tr), f32_mul(tw[1], ti));
				output[(i + j + k - 1) * 2 + 1] = f32_add(f32_mul(tw[0], ti), f32_mul(tw[1], tr));
			}

			tr = f32_add(f32_sub(f32_mul(tw[0], ww[0]), f32_mul(tw[1], ww[1])), tw[0]);
			tw[1] = f32_add(f32_add(f32_mul(tw[0], ww[1]), f32_mul(tw[1], ww[0])), tw[1]);
			tw[0] = tr;
		}

		tempfloat = 2.0;
		memcpy(&tempsoftfloat.v, &tempfloat, sizeof(float));
		if (flg == 0) {
			for (i = 0; i < n * 2; i++) {
				output[i] = f32_div(output[i], tempsoftfloat);
			}
			flg = 1;
		} else if (flg == 1)
			flg = 0;
		k /= 2;
	}

	for (i = j = 0; i < n - 1; i++) {
		if (i < j) {
			tr = output[j * 2];
			ti = output[j * 2 + 1];

			output[j * 2] = output[i * 2];
			output[j * 2 + 1] = output[i * 2 + 1];

			output[i * 2] = tr;
			output[i * 2 + 1] = ti;
		}

		k = n / 2;
		while (k <= j) {
			j = j - k;
			k /= 2;
		}
		j = j + k;
	}
	return;
}
