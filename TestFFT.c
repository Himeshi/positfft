#include <stdio.h>
#include <stdlib.h>
#include "fft.h"

#define FFT_LENGTH 4

int main() {

	float *in, *out;
	float32_t *insf, *outsf;
	int i;

	//generate input and output arrays
	in = (float*) malloc(FFT_LENGTH * 2 * sizeof(float));
	out = (float*) malloc(FFT_LENGTH * 2 * sizeof(float));

	insf = (float32_t*) malloc(FFT_LENGTH * 2 * sizeof(float32_t));
	outsf = (float32_t*) malloc(FFT_LENGTH * 2 * sizeof(float32_t));

	//generate random input
	/*for (i = 0; i < FFT_LENGTH * 2; i++) {
		in[i] = ((float) rand()) / RAND_MAX;
		printf("%f, ", in[i]);
	}
	printf("\n");*/

	in[0] = 0.3784179688;
	in[1] = -0.2729492188;
	in[2] = -0.1494140625;
	in[3] = -0.4252929688;
	in[4] = -0.9521484375;
	in[5] = 0.6093750000;
	in[6] = 0.4091796875;
	in[7] = 0.1635742188;

	//call fft function
	fftFloat(FFT_LENGTH, in, out, 1);

	//call inverse fft function
	fftFloat(FFT_LENGTH, out, in, -1);

	for (i = 0; i < FFT_LENGTH * 2; i++) {
		printf("%f, ", in[i]);
	}
	printf("\n");

	//convert input into softfloat type
	for (i = 0; i < FFT_LENGTH * 2; i++) {
		memcpy(&insf[i].v, &in[i], sizeof(float));
	}

	//call softfloat fft function
	fftSoftFloat(FFT_LENGTH, insf, outsf, 1);

	//call the softfloat inverse fft function
	fftSoftFloat(FFT_LENGTH, outsf, insf, -1);

	//print the output
	float temp;
	for (i = 0; i < FFT_LENGTH * 2; i++) {
		memcpy(&temp, &insf[i], sizeof(float));
		printf("%f, ", temp);
	}

	free(in);
	free(out);

	return 0;
}
