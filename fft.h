#ifndef _fft_h
#define _fft_h

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "softfloat.h"

#define PI 3.14159265

void fftFloat(int n, float* input, float* output, int iflg);

void fftSoftFloat(uint32_t n, float32_t* input, float32_t* output, int iflg);

#endif
