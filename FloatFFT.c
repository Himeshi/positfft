#include "fft.h"

void fftFloat(int n, float* input, float* output, int iflg) {
	int i, j;
	int flg = 0;
	int k = n / 2;
	float tr, ti, th, ww[2], tw[2];

	if (iflg >= 0)
		th = -PI;
	else
		th = PI;

	memcpy(output, input, n * 2 * sizeof(float));

	while (k >= 1) {
		ww[0] = sinf(th / (2 * k)) * sinf(th / (2 * k)) * -2;
		ww[1] = sinf(th / k);

		tw[0] = 1;
		tw[1] = 0;

		for (j = 0; j < k; j++) {
			for (i = 1; i <= n; i += (2 * k)) {
				tr = output[(i + j - 1) * 2] - output[(i + j + k - 1) * 2];
				ti = output[(i + j - 1) * 2 + 1] - output[(i + j + k - 1) * 2 + 1];

				output[(i + j - 1) * 2] = output[(i + j - 1) * 2] + output[(i + j + k - 1) * 2];
				output[(i + j - 1) * 2 + 1] = output[(i + j - 1) * 2 + 1] + output[(i + j + k - 1) * 2 + 1];

				output[(i + j + k - 1) * 2] = tw[0] * tr - tw[1] * ti;
				output[(i + j + k - 1) * 2 + 1] = tw[0] * ti + tw[1] * tr;
			}

			tr = tw[0] * ww[0] - tw[1] * ww[1] + tw[0];
			tw[1] = tw[0] * ww[1] + tw[1] * ww[0] + tw[1];
			tw[0] = tr;
		}

		if (flg == 0) {
			for (i = 0; i < n * 2; i++) {
				output[i] /= 2;
			}
			flg = 1;
		} else if (flg == 1)
			flg = 0;
		k /= 2;
	}

	for (i = j = 0; i < n - 1; i++) {
		if (i < j) {
			tr = output[j * 2];
			ti = output[j * 2 + 1];

			output[j * 2] = output[i * 2];
			output[j * 2 + 1] = output[i * 2 + 1];

			output[i * 2] = tr;
			output[i * 2 + 1] = ti;
		}

		k = n / 2;
		while (k <= j) {
			j = j - k;
			k /= 2;
		}
		j = j + k;
	}

	return;
}
